/* jshint node: true */
'use strict';

var path = require('path');
var mergeTrees = require('broccoli-merge-trees');
var Funnel = require('broccoli-funnel');

module.exports = {
  name: 'ember-openlayers',

  treeForVendor: function(tree) {
      var olPath = path.dirname(require.resolve('openlayers'));
      var olTree = new Funnel(this.treeGenerator(olPath), {
         srcDir: '/',
         destDir: 'openlayers'
      });
      var trees = [];
      if (tree) {
         trees.push(tree);
      };
      trees.push(olTree);
      //return olTree;
      return mergeTrees(trees);
  },

  included: function(app) {
   this._super.included(app);
   if (app.import) {
      app.import('vendor/openlayers/ol.css');
      app.import({
         development: 'vendor/openlayers/ol-debug.js',
         test: 'vendor/openlayers/ol-debug.js',
         production: 'vendor/openlayers/ol.js'
      });
   }
  }

};
